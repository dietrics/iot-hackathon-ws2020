#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

#include "AnotherIFTTTWebhook.h"
#include "WiFi_Credentials.h"

#define IFTTT_API_KEY "c3o0F81wtGS2vXXPbnKM1tBTPY6q50V5oks-uWzBJa7"
#define IFTTT_EVENT_NAME "button_pressed"
#define IFTTT_EVENT2_NAME "button_pressed_survey"
#define IFTTT_EVENT3_NAME "trigger_text_nachricht"
#define USERNAME "Tobias Meinhardt"

// Buttons on D3 & D4 & D5
const int button1 = 0;
const int button2 = 2;
const int mode_button = 14;
int cubeMode = 0;
boolean dataSent = false;

void setup () {
 
  Serial.begin(115200);
  pinMode(button1, INPUT);
  pinMode(button2, INPUT);
  pinMode(mode_button, INPUT);
}


void connectToWifi() {
    while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting...");
  }
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
}
 
void loop() {
  int mode_buttonVal = digitalRead(mode_button);
  if (mode_buttonVal == LOW) { 
    cubeMode++;
    if (cubeMode == 4) {
      cubeMode = 0;
      dataSent = false;
    }
    Serial.println("Modus geändert");
  }

  // Disable WiFi in initial state
  if (cubeMode == 0) {
    WiFi.mode(WIFI_OFF); 
  } 
  else {
    if (WiFi.status() != WL_CONNECTED) {
      WiFi.mode(WIFI_STA);
      WiFi.begin(ssid, password);
      connectToWifi();
    }
    if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
      int button1Val = digitalRead(button1);
      int button2Val = digitalRead(button2);
       
       // Send POST Request to Webook from IFTTT
       // (EVENT, KEY, Value1, Value2, Value3)
      if (button1Val == LOW && cubeMode == 1) { 
        send_webhook(IFTTT_EVENT_NAME, IFTTT_API_KEY, "Ich bin anwesend", USERNAME, "");
        Serial.println("Sende ich bin anwesend");
      }
  
      if (button2Val == LOW && cubeMode == 1) { 
        send_webhook(IFTTT_EVENT_NAME, IFTTT_API_KEY, "Ich muss die Vorlesung verlassen", USERNAME, "");
        Serial.println("Sende ich muss gehen");
      }

      //TODO generate sums for new spreadsheet
      if (button1Val == LOW && cubeMode == 2) { 
        send_webhook(IFTTT_EVENT2_NAME, IFTTT_API_KEY, "JA", USERNAME, "");
        Serial.println("Sende JA");
      }
  
      if (button2Val == LOW && cubeMode == 2) { 
        send_webhook(IFTTT_EVENT2_NAME, IFTTT_API_KEY, "NEIN", USERNAME, "");
        Serial.println("Sende NEIN");
      }

      // Send notification to alexa over ifttt connected to skill "Text Nachricht" that survey is ready to download
      if (cubeMode == 3 && !dataSent) {
        dataSent = true;
        send_webhook(IFTTT_EVENT3_NAME, IFTTT_API_KEY, "", "", "");
        Serial.println("Sende Auswertung fertig"); 
      } 
    } 
  }
  delay(100); 
}
