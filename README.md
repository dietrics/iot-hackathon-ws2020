<h1> WiFi Fidget Cube - Attendance list over button connected to IFTTT</h1>


<a href="">
<img src="" alt="Video" width="560" height="315">
</a>

<h2>Project documentation</h2>

Link to documentation <a href="https://gitlab.reutlingen-university.de/dietrics/iot-hackathon-ws2020/-/wikis/home">Wiki</a>

<h2>Team division</h2>
<table>
<tr><th>Name</th><th>Assignments</th></tr>
<tr><td>Camlice, Tolga</td><td>Representation of topology & infrastructure  <br/> Pro's & Con's of the project concept</td></tr>
<tr><td>Dietrich, Sabine</td><td>Project creation in GitLab with structuring  <br/> Documentation </td></tr>
<tr><td>Meinhardt, Tobias Rico</td><td>Preparation of software environment & MVP (Code + Description) <br/> Hardware <br/> Explanation video</td></tr>
<tr><td>Prange, Sonja</td><td>Design principles <br/> Short presentation</td></tr>
<tr><td>Together</td><td>Registration and establishment of needed services <br/> Coding <br/> Testing</td></tr>
</table>


